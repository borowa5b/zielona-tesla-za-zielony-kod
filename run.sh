#!/bin/sh

jarFile="build/quarkus-app/quarkus-run.jar"

run_app() {
  java -jar "$jarFile"
}

if [ ! -f "$jarFile" ]; then
	sh build.sh
	run_app
else
  run_app
fi
