package pl.borowski.game.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

public record Order(@JsonValue List<Group> groups) {

    public Order() {
        this(new ArrayList<>());
    }

    public void addGroup(Group group) {
        groups.add(group);
    }
}
