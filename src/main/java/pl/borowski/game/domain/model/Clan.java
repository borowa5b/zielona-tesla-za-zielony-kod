package pl.borowski.game.domain.model;

import javax.ws.rs.BadRequestException;

public record Clan(int numberOfPlayers, int points) {

    private static final int MAX_CLAN_COUNT = 1000;
    private static final int MAX_SCORE_COUNT = 1000000;

    public void validate() {
        if (numberOfPlayers > MAX_CLAN_COUNT) {
            throw new BadRequestException("Number of players cannot be greater than " + MAX_CLAN_COUNT);
        }
        if (points > MAX_SCORE_COUNT) {
            throw new BadRequestException("Points cannot be greater than " + MAX_SCORE_COUNT);
        }
    }
}
