package pl.borowski.game.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

public record Group(@JsonValue List<Clan> clans) {

    public Group() {
        this(new ArrayList<>());
    }

    public void addClan(Clan clan) {
        this.clans.add(clan);
    }

    public int getPlayerCount() {
        return this.clans.stream()
                         .map(Clan::numberOfPlayers)
                         .reduce(0, Integer::sum);
    }
}
