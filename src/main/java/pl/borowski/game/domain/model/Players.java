package pl.borowski.game.domain.model;

import javax.ws.rs.BadRequestException;
import java.util.Comparator;
import java.util.List;

public record Players(int groupCount, List<Clan> clans) {

    private static final int MAX_GROUP_COUNT = 1000;
    private static final int MAX_CLAN_COUNT = 20000;

    public void sortClansByPointsDesc() {
        clans.sort(Comparator.comparing(Clan::points)
                             .reversed());
    }

    public void validate() {
        if (groupCount > MAX_GROUP_COUNT) {
            throw new BadRequestException(String.format("Group count must be less than %d", MAX_GROUP_COUNT));
        }
        if (clans == null || clans.isEmpty()) {
            throw new BadRequestException("Clan list should not be empty");
        }
        if (clans.size() > MAX_CLAN_COUNT) {
            throw new BadRequestException(String.format("Clan count must be less than %d", MAX_CLAN_COUNT));
        }
        clans.forEach(Clan::validate);
    }
}
