package pl.borowski.game.domain;

import pl.borowski.game.domain.model.Clan;
import pl.borowski.game.domain.model.Group;
import pl.borowski.game.domain.model.Order;
import pl.borowski.game.domain.model.Players;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

@ApplicationScoped
public class ClanOrderCalculator {

    public Order calculate(final Players players) {
        players.sortClansByPointsDesc();
        return assignClansToGroups(players);
    }

    private Order assignClansToGroups(final Players players) {
        final var maxPlayersInGroups = players.groupCount();
        final var playersClans = players.clans();
        final var order = new Order();

        while (!playersClans.isEmpty()) {
            final var group = new Group();
            final var toRemove = new ArrayList<Clan>();

            playersClans.stream()
                        .takeWhile(groupIsNotFilled(maxPlayersInGroups, group))
                        .forEach(clan -> {
                            if (shouldSkipClan(maxPlayersInGroups, group, clan)) {
                                return;
                            }

                            addClanToGroup(playersClans, group, toRemove, clan);
                        });

            order.addGroup(group);
            playersClans.removeAll(toRemove);
        }
        return order;
    }

    private Predicate<Clan> groupIsNotFilled(final int maxPlayersInGroups, final Group group) {
        return clan -> group.getPlayerCount() < maxPlayersInGroups;
    }

    private boolean shouldSkipClan(final int maxPlayersInGroups, final Group group, final Clan clan) {
        return clan.numberOfPlayers() > maxPlayersInGroups
               || group.getPlayerCount() + clan.numberOfPlayers() > maxPlayersInGroups;
    }

    private Optional<Clan> findNextClanWithTheSamePoints(final List<Clan> playersClans, final Clan clan) {
        return playersClans.stream()
                           .filter(c -> playersClans.indexOf(c) != playersClans.indexOf(clan)
                                        && c.points() == clan.points())
                           .findFirst();
    }

    private void addClanToGroup(final List<Clan> playersClans,
                                final Group group,
                                final ArrayList<Clan> toRemove,
                                final Clan clan) {
        final var nextClanWithTheSamePointsOptional = findNextClanWithTheSamePoints(playersClans, clan);
        final var nextClanWithTheSamePointsHasLessPoints = nextClanWithTheSamePointsOptional.isPresent()
                                                           && nextClanWithTheSamePointsOptional.get()
                                                                                               .numberOfPlayers()
                                                              < clan.numberOfPlayers();
        if (nextClanWithTheSamePointsHasLessPoints) {
            final var nextClanWithTheSamePoints = nextClanWithTheSamePointsOptional.get();
            group.addClan(nextClanWithTheSamePoints);
            toRemove.add(nextClanWithTheSamePoints);
        } else {
            group.addClan(clan);
            toRemove.add(clan);
        }
    }
}
