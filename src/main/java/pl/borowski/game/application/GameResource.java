package pl.borowski.game.application;

import io.smallrye.common.annotation.NonBlocking;
import pl.borowski.game.domain.ClanOrderCalculator;
import pl.borowski.game.domain.model.Players;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/onlinegame")
public class GameResource {

    private final ClanOrderCalculator clanOrderCalculator;

    @Inject
    public GameResource(final ClanOrderCalculator clanOrderCalculator) {
        this.clanOrderCalculator = clanOrderCalculator;
    }

    @POST
    @NonBlocking
    @Path("/calculate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculate(final Players players) {
        validate(players);
        return Response.ok()
                       .entity(clanOrderCalculator.calculate(players))
                       .build();
    }

    private void validate(final Players players) {
        if (players == null) {
            throw new BadRequestException("Players cannot be null");
        }
        players.validate();
    }
}
