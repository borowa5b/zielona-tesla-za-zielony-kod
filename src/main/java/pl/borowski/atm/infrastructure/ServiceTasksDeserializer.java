package pl.borowski.atm.infrastructure;

import pl.borowski.atm.domain.model.ServiceTasks;
import pl.borowski.atm.domain.model.Task;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Arrays;

public class ServiceTasksDeserializer extends JsonDeserializer<ServiceTasks> {

    @Override
    public ServiceTasks deserialize(final JsonParser p, final DeserializationContext ctxt) throws IOException {
        final var list = ctxt.readValue(p, Task[].class);
        return new ServiceTasks(Arrays.asList(list));
    }
}
