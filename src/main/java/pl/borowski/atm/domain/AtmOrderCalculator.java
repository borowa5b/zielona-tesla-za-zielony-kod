package pl.borowski.atm.domain;

import io.vertx.core.impl.ConcurrentHashSet;
import static java.util.stream.Collectors.groupingBy;
import pl.borowski.atm.domain.model.ATM;
import pl.borowski.atm.domain.model.Order;
import pl.borowski.atm.domain.model.ServiceTasks;
import pl.borowski.atm.domain.model.Task;

import javax.enterprise.context.ApplicationScoped;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@ApplicationScoped
public class AtmOrderCalculator {

    public Order calculate(final ServiceTasks serviceTasks) {
        final var tasks = serviceTasks.tasks();
        serviceTasks.sortByRegionAsc();

        final var tasksByRegion = groupTasksByRegion(tasks);
        return orderedServiceTasks(tasksByRegion);
    }

    private static Order orderedServiceTasks(final Map<Integer, List<Task>> tasksByRegion) {
        final var order = new Order();

        tasksByRegion.forEach((region, regionTasks) -> {
            final var regionTasksWithoutDuplicated = regionTasks.stream()
                                                                .filter(distinctBy(Task::atmId))
                                                                .toList();
            final var regionTasksByPriority = regionTasksWithoutDuplicated.stream()
                                                                          .sorted(Comparator.comparing(Task::requestType))
                                                                          .toList();
            order.addAtms(regionTasksByPriority.stream()
                                               .map(task -> new ATM(task.region(), task.atmId()))
                                               .collect(Collectors.toList()));
        });

        return order;
    }

    private static Map<Integer, List<Task>> groupTasksByRegion(final List<Task> tasks) {
        return tasks.stream()
                    .collect(groupingBy(Task::region));
    }

    public static <T> Predicate<T> distinctBy(final Function<? super T, ?> f) {
        final var objects = new ConcurrentHashSet<>();
        return t -> objects.add(f.apply(t));
    }
}
