package pl.borowski.atm.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

public record Order(@JsonValue List<ATM> atms) {

    public Order() {
        this(new ArrayList<>());
    }

    public void addAtms(final List<ATM> atm) {
        atms.addAll(atm);
    }
}
