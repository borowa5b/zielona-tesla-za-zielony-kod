package pl.borowski.atm.domain.model;

import javax.ws.rs.BadRequestException;

public record Task(int region, RequestType requestType, int atmId) {

    public void validate() {
        if (region > 9999) {
            throw new BadRequestException("Region should be less than 10000");
        }
        if (atmId > 9999) {
            throw new BadRequestException("Atm id should be less than 10000");
        }
    }
}
