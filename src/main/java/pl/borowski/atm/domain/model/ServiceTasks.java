package pl.borowski.atm.domain.model;

import javax.ws.rs.BadRequestException;
import java.util.Comparator;
import java.util.List;

public record ServiceTasks(List<Task> tasks) {

    public void sortByRegionAsc() {
        tasks.sort(Comparator.comparing(Task::region));
    }

    public void validate() {
        if (tasks == null || tasks.isEmpty()) {
            throw new BadRequestException("Tasks cannot be empty");
        }
        tasks.forEach(Task::validate);
    }
}
