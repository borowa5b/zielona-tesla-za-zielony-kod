package pl.borowski.atm.domain.model;

/**
 * Request type for service task. Order of declarations is the order used for sorting given region tasks (based on ATM
 * task description)
 */
public enum RequestType {
    FAILURE_RESTART, // 1 - biggest priority task
    PRIORITY, // 2
    SIGNAL_LOW, // 3
    STANDARD, // 4 - lowest priority task
}
