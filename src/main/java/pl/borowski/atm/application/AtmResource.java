package pl.borowski.atm.application;

import io.smallrye.common.annotation.NonBlocking;
import pl.borowski.atm.domain.AtmOrderCalculator;
import pl.borowski.atm.domain.model.ServiceTasks;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/atms")
public class AtmResource {

    private final AtmOrderCalculator atmOrderCalculator;

    @Inject
    public AtmResource(final AtmOrderCalculator atmOrderCalculator) {
        this.atmOrderCalculator = atmOrderCalculator;
    }

    @POST
    @NonBlocking
    @Path("/calculateOrder")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response calculate(final ServiceTasks serviceTasks) {
        validate(serviceTasks);
        return Response.ok()
                       .entity(atmOrderCalculator.calculate(serviceTasks))
                       .build();
    }

    private void validate(final ServiceTasks serviceTasks) {
        if (serviceTasks == null) {
            throw new BadRequestException("Service tasks cannot be null");
        }
        serviceTasks.validate();
    }
}
