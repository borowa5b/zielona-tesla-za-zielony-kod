package pl.borowski.transactions.application;

import io.smallrye.common.annotation.NonBlocking;
import pl.borowski.transactions.domain.ReportExecutor;
import pl.borowski.transactions.domain.model.Transactions;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/transactions")
public class TransactionsResource {

    private final ReportExecutor reportExecutor;

    @Inject
    public TransactionsResource(final ReportExecutor reportExecutor) {
        this.reportExecutor = reportExecutor;
    }

    @POST
    @NonBlocking
    @Path("/report")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response executeReport(final Transactions transactions) {
        validate(transactions);
        return Response.ok()
                       .entity(reportExecutor.execute(transactions))
                       .build();
    }

    private void validate(final Transactions transactions) {
        if (transactions == null) {
            throw new BadRequestException("Transactions cannot be null");
        }
        transactions.validate();
    }
}
