package pl.borowski.transactions.infrastructure;

import pl.borowski.transactions.domain.model.Transaction;
import pl.borowski.transactions.domain.model.Transactions;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Arrays;

public class TransactionsDeserializer extends JsonDeserializer<Transactions> {

    @Override
    public Transactions deserialize(final JsonParser p, final DeserializationContext ctxt) throws IOException {
        final var list = ctxt.readValue(p, Transaction[].class);
        return new Transactions(Arrays.asList(list));
    }
}
