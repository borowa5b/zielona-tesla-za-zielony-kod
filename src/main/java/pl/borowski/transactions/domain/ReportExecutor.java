package pl.borowski.transactions.domain;

import pl.borowski.transactions.domain.model.Accounts;
import pl.borowski.transactions.domain.model.Transactions;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ReportExecutor {

    public Accounts execute(final Transactions transactions) {
        final var accounts = new Accounts();
        final var transactionsList = transactions.transactions();
        accounts.addAccounts(transactionsList);
        transactionsList.forEach(accounts::addTransaction);
        accounts.sortByAccountNumber();
        return accounts;
    }
}
