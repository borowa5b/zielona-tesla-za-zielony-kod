package pl.borowski.transactions.domain.model;

import javax.ws.rs.BadRequestException;
import java.util.List;

public record Transactions(List<Transaction> transactions) {

    public void validate() {
        if (transactions == null || transactions.isEmpty()) {
            throw new BadRequestException("Transactions list cannot be empty");
        }
        if (transactions.size() > 10000) {
            throw new BadRequestException("Too many transactions. Limit is 10000.");
        }
        transactions.forEach(Transaction::validate);
    }
}
