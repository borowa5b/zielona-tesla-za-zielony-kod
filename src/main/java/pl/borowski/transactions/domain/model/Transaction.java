package pl.borowski.transactions.domain.model;

import javax.ws.rs.BadRequestException;

public record Transaction(String debitAccount, String creditAccount, float amount) {

    public void validate() {
        if (debitAccount.length() != 26) {
            throw new BadRequestException("Debit account number must be 26 characters long");
        }
        if (creditAccount.length() != 26) {
            throw new BadRequestException("Credit account number must be 26 characters long");
        }
    }
}
