package pl.borowski.transactions.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Account {

    private final String account;
    private int debitCount = 0;
    private int creditCount = 0;
    private float balance = 0;

    @JsonIgnore
    private final DecimalFormat decimalFormat;

    public Account(final String account) {
        this.account = account;
        decimalFormat = new DecimalFormat("0.00");
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
    }

    public String getAccount() {
        return account;
    }

    public int getDebitCount() {
        return debitCount;
    }

    public int getCreditCount() {
        return creditCount;
    }

    public float getBalance() {
        return balance;
    }

    public void update(final Operation operation, final float amount) {
        switch (operation) {
            case DEBIT -> {
                debitCount += 1;
                balance = round(balance -= amount);
            }
            case CREDIT -> {
                creditCount += 1;
                balance = round(balance += amount);
            }
        }
    }

    private float round(final float balance) {
        return Float.parseFloat(decimalFormat.format(balance));
    }
}
