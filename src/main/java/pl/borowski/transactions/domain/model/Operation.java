package pl.borowski.transactions.domain.model;

public enum Operation {
    CREDIT, DEBIT
}
