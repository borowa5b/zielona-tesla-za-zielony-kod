package pl.borowski.transactions.domain.model;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public record Accounts(@JsonValue List<Account> accounts) {

    public Accounts() {
        this(new ArrayList<>());
    }

    public void addAccounts(final List<Transaction> transactions) {
        transactions.forEach(transaction -> {
            if (doesNotContainsAccount(transaction.creditAccount())) {
                accounts.add(new Account(transaction.creditAccount()));
            }
            if (doesNotContainsAccount(transaction.debitAccount())) {
                accounts.add(new Account(transaction.debitAccount()));
            }
        });
    }

    public void addTransaction(final Transaction transaction) {
        final var debitAccount = transaction.debitAccount();
        final var creditAccount = transaction.creditAccount();
        final var amount = transaction.amount();
        updateAccount(Operation.DEBIT, debitAccount, amount);
        updateAccount(Operation.CREDIT, creditAccount, amount);
    }

    public void sortByAccountNumber() {
        accounts.sort(Comparator.comparing(Account::getAccount));
    }

    private void updateAccount(final Operation operation, final String account, final float amount) {
        accounts.stream()
                .filter(a -> a.getAccount()
                              .equals(account))
                .findFirst()
                .ifPresent(a -> a.update(operation, amount));
    }

    private boolean doesNotContainsAccount(final String accountNumber) {
        return accounts.stream()
                       .noneMatch(account -> account.getAccount()
                                                    .equals(accountNumber));
    }
}
