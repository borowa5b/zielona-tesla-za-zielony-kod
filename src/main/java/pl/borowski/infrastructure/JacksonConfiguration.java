package pl.borowski.infrastructure;

import io.quarkus.jackson.ObjectMapperCustomizer;
import pl.borowski.atm.domain.model.ServiceTasks;
import pl.borowski.atm.infrastructure.ServiceTasksDeserializer;
import pl.borowski.transactions.domain.model.Transactions;
import pl.borowski.transactions.infrastructure.TransactionsDeserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class JacksonConfiguration implements ObjectMapperCustomizer {

    @Override
    public void customize(ObjectMapper objectMapper) {
        final var module = new SimpleModule();
        module.addDeserializer(ServiceTasks.class, new ServiceTasksDeserializer());
        module.addDeserializer(Transactions.class, new TransactionsDeserializer());
        objectMapper.registerModule(module);
    }
}
