package pl.borowski.utils;

import io.quarkus.logging.Log;

import com.google.common.io.Resources;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileUtils {

    public static String readFileWithoutWhiteSpaces(final String filePath) {
        try {
            return Resources.toString(Resources.getResource(filePath), StandardCharsets.UTF_8)
                            .replaceAll("\\s", "");
        } catch (final IOException e) {
            Log.error("Failed to read file " + filePath);
        }
        return null;
    }
}
