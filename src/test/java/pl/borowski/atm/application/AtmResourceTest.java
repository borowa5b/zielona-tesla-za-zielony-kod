package pl.borowski.atm.application;

import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import static org.hamcrest.CoreMatchers.is;
import static pl.borowski.utils.FileUtils.readFileWithoutWhiteSpaces;

import org.junit.jupiter.api.Test;

@QuarkusTest
class AtmResourceTest {

    @Test
    void shouldCalculateAtmOrderForFirstExampleRequest() {
        final var request = readFileWithoutWhiteSpaces("request/atm/AtmRequest1.json");
        final var response = readFileWithoutWhiteSpaces("response/atm/AtmResponse1.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/atms/calculateOrder")
               .then()
               .statusCode(200)
               .body(is(response));
    }

    @Test
    void shouldCalculateAtmOrderForSecondExampleRequest() {
        final var request = readFileWithoutWhiteSpaces("request/atm/AtmRequest2.json");
        final var response = readFileWithoutWhiteSpaces("response/atm/AtmResponse2.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/atms/calculateOrder")
               .then()
               .statusCode(200)
               .body(is(response));
    }

    @Test
    void shouldReturn400WhenThereIsNoServiceTasks() {
        final var request = readFileWithoutWhiteSpaces("request/atm/AtmRequestNoServiceTasks.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/atms/calculateOrder")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400WhenTasksAreEmpty() {
        final var request = readFileWithoutWhiteSpaces("request/atm/AtmRequestEmptyTasks.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/atms/calculateOrder")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400WhenAtmRequestRegionNumberIsTooBig() {
        final var request = readFileWithoutWhiteSpaces("request/atm/AtmRequestRegionNumberTooBig.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/atms/calculateOrder")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400WhenAtmRequestAtmNumberIsTooBig() {
        final var request = readFileWithoutWhiteSpaces("request/atm/AtmRequestAtmNumberTooBig.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/atms/calculateOrder")
               .then()
               .statusCode(400);
    }
}
