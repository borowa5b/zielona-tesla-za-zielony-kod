package pl.borowski.game.application;

import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import static org.hamcrest.CoreMatchers.is;
import static pl.borowski.utils.FileUtils.readFileWithoutWhiteSpaces;

import org.junit.jupiter.api.Test;

@QuarkusTest
class GameResourceTest {

    @Test
    void shouldCalculateClanOrder() {
        final var request = readFileWithoutWhiteSpaces("request/game/GameRequest.json");
        final var response = readFileWithoutWhiteSpaces("response/game/GameResponse.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/onlinegame/calculate")
               .then()
               .statusCode(200)
               .body(is(response));
    }

    @Test
    void shouldReturn400HttpStatusWhenThereIsNoPlayers() {
        final var request = readFileWithoutWhiteSpaces("request/game/GameRequestNoPlayers.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/onlinegame/calculate")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400HttpStatusWhenThereIsNoClans() {
        final var request = readFileWithoutWhiteSpaces("request/game/GameRequestNoClans.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/onlinegame/calculate")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400HttpStatusWhenThereIsTooBigGroupCount() {
        final var request = readFileWithoutWhiteSpaces("request/game/GameRequestTooBigGroupCount.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/onlinegame/calculate")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400HttpStatusWhenThereIsTooMuchClans() {
        final var request = readFileWithoutWhiteSpaces("request/game/GameRequestTooMuchClans.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/onlinegame/calculate")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400HttpStatusWhenThereIsTooMuchPlayersInClan() {
        final var request = readFileWithoutWhiteSpaces("request/game/GameRequestTooMuchPlayersInClan.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/onlinegame/calculate")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400HttpStatusWhenThereIsTooBigAmountOfPointsInClan() {
        final var request = readFileWithoutWhiteSpaces("request/game/GameRequestTooBigAmountOfPointsInClan.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/onlinegame/calculate")
               .then()
               .statusCode(400);
    }
}
