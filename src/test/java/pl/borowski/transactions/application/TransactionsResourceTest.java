package pl.borowski.transactions.application;

import io.quarkus.test.junit.QuarkusTest;
import static io.restassured.RestAssured.given;
import io.restassured.http.ContentType;
import static org.hamcrest.CoreMatchers.is;
import static pl.borowski.utils.FileUtils.readFileWithoutWhiteSpaces;

import org.junit.jupiter.api.Test;

@QuarkusTest
class TransactionsResourceTest {

    @Test
    void shouldExecuteReport() {
        final var request = readFileWithoutWhiteSpaces("request/transactions/TransactionsRequest.json");
        final var response = readFileWithoutWhiteSpaces("response/transactions/TransactionsResponse.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/transactions/report")
               .then()
               .statusCode(200)
               .body(is(response));
    }

    @Test
    void shouldReturn400WhenThereIsNoTransactions() {
        final var request = readFileWithoutWhiteSpaces("request/transactions/TransactionsRequestNullTransactions.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/transactions/report")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400WhenThereIsTooManyTransactions() {
        final var request = readFileWithoutWhiteSpaces("request/transactions/TransactionsRequestTooManyTransactions.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/transactions/report")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400WhenThereIsWrongCreditAccountNumber() {
        final var request = readFileWithoutWhiteSpaces("request/transactions/TransactionsRequestWrongCreditAccount.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/transactions/report")
               .then()
               .statusCode(400);
    }

    @Test
    void shouldReturn400WhenThereIsWrongDebitAccountNumber() {
        final var request = readFileWithoutWhiteSpaces("request/transactions/TransactionsRequestWrongDebitAccount.json");
        given().when()
               .contentType(ContentType.JSON)
               .body(request)
               .post("/transactions/report")
               .then()
               .statusCode(400);
    }
}
